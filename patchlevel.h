/*
 * patchlevel.h: Our life story.
 */
#ifndef _h_patchlevel
#define _h_patchlevel

#define ORIGIN "Astron"
#define REV 6
#define VERS 23
#define PATCHLEVEL 00
#define DATE "2021-11-11"

#endif /* _h_patchlevel */
